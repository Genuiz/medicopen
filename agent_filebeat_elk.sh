#!/bin/bash

# https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elastic-stack-on-ubuntu-20-04-fr
# https://testsoft.net/filebeat-install-and-config-remote-log-shiping/

curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list


sudo apt update
sudo apt install default-jre -y

java -version
sudo apt install default-jdk -y
javac -version

sudo apt install nginx -y

echo "Étape 2 — Installation et configuration de Logstash"
sudo apt install logstash -y

sudo touch /etc/logstash/conf.d/02-beats-input.conf
sudo chmod 667 /etc/logstash/conf.d/02-beats-input.conf

echo "
input {
  beats {
    port => 5044
  }
}
" > /etc/logstash/conf.d/02-beats-input.conf

sudo touch  /etc/logstash/conf.d/30-elasticsearch-output.conf
sudo chmod 667 /etc/logstash/conf.d/30-elasticsearch-output.conf

sudo echo '
output {
  if [@metadata][pipeline] {
	elasticsearch {
  	hosts => ["192.168.33.112:9200"]
  	manage_template => false
  	index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
  	pipeline => "%{[@metadata][pipeline]}"
	}
  } else {
	elasticsearch {
  	hosts => ["192.168.33.112:9200"]
  	manage_template => false
  	index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
	}
  }
}
' > /etc/logstash/conf.d/30-elasticsearch-output.conf

sudo -u logstash /usr/share/logstash/bin/logstash --path.settings /etc/logstash -t

sudo systemctl start logstash
sudo systemctl enable logstash

echo "Étape 3 — Installation et configuration de Filebeat"

sudo apt install filebeat -y

# Todo edit /etc/filebeat/filebeat.yml choisir l'utlisation de logostash ou elasticsearch

sudo filebeat modules enable system
sudo filebeat modules list
sudo filebeat setup --pipelines --modules system
sudo filebeat setup --index-management -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["192.168.33.112:9200"]'
# le remplacer avec l'adresse ip du server elk
#sudo filebeat setup -E output.logstash.enabled=false -E output.elasticsearch.hosts=['192.168.33.112:9200'] -E setup.kibana.host=localhost:5601
sudo filebeat setup -E output.logstash.enabled=false -E output.elasticsearch.hosts=['192.168.33.112:9200'] -E setup.kibana.host=192.168.33.112:90

sudo systemctl start filebeat
sudo systemctl enable filebeat

curl -XGET 'http://192.168.33.112:9200/filebeat-*/_search?pretty'
