#!/bin/bash

#https://docs.openstack.org/ocata/user-guide/cli-create-and-manage-networks.html
echo "Bienvenue dans le deploiement de l'infra medicarche"

sudo snap install microstack --edge --devmode
snap list microstack
sudo microstack init --auto --control
sudo apt install git -y
sudo apt install sshpass -y

#https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html
#https://docs.gitlab.com/ee/user/public_access.html

#git config --global user.email "mulsben7@gmail.com"
#git config --global user.name "Genuiz"

git clone https://gitlab.com/Genuiz/os-ubuntu-openstack.git

#microstack.openstack --os-username=admin --os-password=keystone image create --file ./path_to_an_image.img --public --container-format bare --disk-format qcow2 'MyImage'
#microstack.openstack image create  --os-image-api-version 2 --protected False --min-disk 1 --disk-format qcow2 --min-ram 256  --file ./focal-server-cloudimg-amd64.img 'ubuntu'
microstack.openstack image create --container-format bare --disk-format qcow2 --file ./os-ubuntu-openstack/focal-server-cloudimg-amd64.img ubuntu2
#microstack.openstack image create --disk-format qcow2 --public --file ./focal-server-cloudimg-amd64.img ubuntu
microstack.openstack flavor create m3.custom --id auto --ram 1024 --disk 5 --vcpus 1
microstack.openstack flavor create m4.custom --id auto --ram 1024 --disk 8 --vcpus 1
microstack.openstack flavor create m5.custom --id auto --ram 2048 --disk 12 --vcpus 2
microstack.openstack image list
microstack.openstack flavor list

microstack.openstack security group create private-sg
microstack.openstack security group rule create private-sg --ingress --protocol tcp --dst-port 8085:8085 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg --ingress --protocol tcp --dst-port 8086:8086 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg  --ingress --protocol tcp --dst-port 8087:8087 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg  --ingress --protocol tcp --dst-port 22:22 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg  --ingress --protocol tcp --dst-port 80:80 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create --protocol icmp private-sg

microstack.openstack security group rule list

microstack launch cirros -n test
sudo iptables -t nat -A POSTROUTING -s 10.20.20.1/24 ! -d 10.20.20.1/24 -j MASQUERADE
sudo sysctl net.ipv4.ip_forward=1


sudo snap get microstack config.credentials.keystone-password


git clone https://gitlab.com/Genuiz/medicopen.git
#cd medicopen
source ./medicopen/auto.sh

sudo snap get microstack config.credentials.keystone-password
