#!/bin/bash

#https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elastic-stack-on-ubuntu-20-04-fr

#https://terryl.in/en/elasticsearch-service-start-operation-timed-out/

#https://devopssec.fr/article/comprendre-utiliser-packetbeat-stack-elk

#https://www.elastic.co/fr/webinars/x-pack-security-webinar-french
#https://grafana.com/oss/prometheus/exporters/postgres-exporter/?tab=installation

#https://blog.ruanbekker.com/blog/2020/04/28/nginx-analysis-dashboard-using-grafana-and-elasticsearch/

#https://openclassrooms.com/fr/courses/1750566-optimisez-la-securite-informatique-grace-au-monitoring/7145362-installez-elk


echo "##########################################################"
echo "# Etape 1 Installation des prerequis et de elasticsearch #"
echo "##########################################################"

curl -fsSL https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list
sudo apt update
sudo apt install elasticsearch -y

#sudo nano /etc/elasticsearch/elasticsearch.yml
sudo sed -i '/#network.host: 192.168.0.1/c\network.host: 0.0.0.0' /etc/elasticsearch/elasticsearch.yml  
sudo sed -i '/#node.name: node-1/c\node.name: node-1' /etc/elasticsearch/elasticsearch.yml   
sudo sed -i '75i cluster.initial_master_nodes: ["node-1"]' /etc/elasticsearch/elasticsearch.yml


sudo systemctl start elasticsearch
sudo systemctl enable elasticsearch

curl -X GET "localhost:9200"

echo "##########################################################"
echo "# Etape 2 Installation de Kibana                         #"
echo "##########################################################"
sudo apt install kibana -y
sudo systemctl enable kibana
sudo systemctl start kibana
sudo apt install nginx -y
echo "MyStrongCryptedPassword" > filepass.txt
echo "kibanaadmin:`openssl passwd -in filepass.txt -apr1`" | sudo tee -a /etc/nginx/htpasswd.users
sudo touch /etc/nginx/sites-available/elk_medicarche
sudo chmod 777 /etc/nginx/sites-available/elk_medicarche
sudo echo "
server {
    listen 90;

    server_name elk_medicarche;

    #auth_basic "Restricted Access";
    auth_basic_user_file /etc/nginx/htpasswd.users;

    location / {
        proxy_pass http://localhost:5601;
        proxy_http_version 1.1;
        #proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection 'upgrade';
        #proxy_set_header Host $host;
        #proxy_cache_bypass $http_upgrade;
    }
}
" > /etc/nginx/sites-available/elk_medicarche
sudo ln -s /etc/nginx/sites-available/elk_medicarche /etc/nginx/sites-enabled/elk_medicarche
sudo nginx -t
sudo systemctl reload nginx

echo "##########################################################"
echo "# Etape 3 Installation de Logstash                       #"
echo "##########################################################"
sudo apt install logstash -y
sudo touch /etc/logstash/conf.d/02-beats-input.conf
sudo chmod 777 /etc/logstash/conf.d/02-beats-input.conf
sudo echo "
input {
  beats {
    port => 5044
  }
}
" > /etc/logstash/conf.d/02-beats-input.conf
sudo touch  /etc/logstash/conf.d/30-elasticsearch-output.conf
sudo chmod 777 /etc/logstash/conf.d/30-elasticsearch-output.conf
sudo echo '
output {
  if [@metadata][pipeline] {
elasticsearch {
  hosts => ["localhost:9200"]
  manage_template => false
  index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
  pipeline => "%{[@metadata][pipeline]}"
}
  } else {
elasticsearch {
  hosts => ["localhost:9200"]
  manage_template => false
  index => "%{[@metadata][beat]}-%{[@metadata][version]}-%{+YYYY.MM.dd}"
}
  }
}
' > /etc/logstash/conf.d/30-elasticsearch-output.conf
sudo -u logstash /usr/share/logstash/bin/logstash --path.settings /etc/logstash -t
sudo systemctl start logstash
sudo systemctl enable logstash

echo "##########################################################"
echo "# Etape 4 Installation de l'agent Filebeat               #"
echo "##########################################################"
sudo apt install filebeat -y
# sudo nano /etc/filebeat/filebeat.yml Edit fileBeat à la mano
sudo filebeat modules enable system
sudo filebeat modules list
sudo filebeat setup --pipelines --modules system
sudo filebeat setup --index-management -E output.logstash.enabled=false -E 'output.elasticsearch.hosts=["localhost:9200"]'
sudo filebeat setup -E output.logstash.enabled=false -E output.elasticsearch.hosts=['localhost:9200'] -E setup.kibana.host=localhost:5601
sudo systemctl start filebeat
sudo systemctl enable filebeat
curl -XGET 'http://localhost:9200/filebeat-*/_search?pretty'

echo "##########################################################"
echo "# Etape 5 Installation de l'agent PacketBeat             #"
echo "##########################################################"

sudo apt install apt-transport-https -y
sudo apt update && sudo apt install packetbeat -y
sudo systemctl start packetbeat
sudo packetbeat setup



