#!/bin/bash

sudo apt update
#sudo apt install apache2 -y
sudo a2enmod proxy proxy_http headers proxy_wstunnel
sudo systemctl restart apache2
sudo touch /etc/apache2/sites-available/odoo.conf
sudo chmod 777 /etc/apache2/sites-available/odoo.conf
sudo a2ensite odoo.conf
sudo sed -i '$ a Listen 8087' /etc/apache2/ports.conf
sudo systemctl restart apache2

#adresse ip à modifier
echo "<VirtualHost *:8087>
        ServerName localhost
        #ProxyPreserveHost On
        ProxyPass / http://$ip_vm3:8069/
        ProxyPassReverse / http://$ip_vm3:8069/
      </VirtualHost>" > /etc/apache2/sites-available/odoo.conf

sudo service apache2 restart
sudo lsof -i -P -n | grep LISTEN

