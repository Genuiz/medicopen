#!/bin/bash

#mise à jour du systeme 
sudo apt update

#installation du serveur apache et de la base de donnée mariadb ansi que les dependances
sudo apt install -y apache2 mariadb-server libapache2-mod-php7.4
sudo apt install -y php7.4-gd php7.4-mysql php7.4-curl php7.4-mbstring php7.4-intl
sudo apt install -y php7.4-gmp php7.4-bcmath php-imagick php7.4-xml php7.4-zip
sudo /etc/init.d/mysql start
#systemctl status mysql

#création de l'utilisateur nextclouduser, du mot de pass, de la base de donnée et attribution des privillège sur la db
sudo mysql -u root --execute  "CREATE USER 'nextclouduser'@'localhost' IDENTIFIED BY 'password';
CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextclouduser'@'localhost';
FLUSH PRIVILEGES; "

#Telechargement de nextcloud, decompression et deplacement vers le repertoire des projets web
wget https://download.nextcloud.com/server/releases/latest.tar.bz2
tar -xvf latest.tar.bz2 nextcloud/
sudo cp -r nextcloud /var/www
sudo touch /etc/apache2/sites-available/nextcloud.conf
#En raison de test j'utilise le 777. Dans la prod je le channgerai en 557

#activation et configuration du serveur virtuel apache pour que le site soit accecible depuis le port 9000
sudo chmod 777 /etc/apache2/sites-available/nextcloud.conf
echo "<VirtualHost *:9000>
  DocumentRoot /var/www/nextcloud/
  ServerName localhost
  
  <Directory /var/www/nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
  </VirtualHost>"  > /etc/apache2/sites-available/nextcloud.conf

#attribution des droits abusivement (755 à la place de 777) pour faciliter la modification avec la commande sed
sudo chmod 777 /etc/apache2/ports.conf
#sudo echo "Listen 9000" >> /etc/apache2/ports.conf
sudo sed -i '$ a Listen 9000' /etc/apache2/ports.conf

#activation des modules apache et redemarrage du serveur
sudo a2ensite nextcloud.conf
sudo a2enmod rewrite
sudo systemctl reload apache2
sudo systemctl restart apache2
sudo a2enmod headers
sudo a2enmod env
sudo a2enmod dir
sudo a2enmod mime
sudo chown -R www-data:www-data /var/www/nextcloud/
sudo systemctl reload apache2
