#!/bin/bash

sudo apt-get update -y
sudo apt-get install gnupg2 curl apt-transport-https -y
curl -s https://syncthing.net/release-key.txt | apt-key add -

sudo touch /etc/apt/sources.list.d/syncthing.list
sudo chmod 777 /etc/apt/sources.list.d/syncthing.list
# FILE=/etc/apt/sources.list.d/syncthing.list

# if [ -f "$FILE" ]; then
#     echo "$FILE exists."
# else 
   sudo touch /etc/systemd/system/syncthing@.service
   #En raison de test j'utilise le 777. Dans la prod je le channgerai en 557
   sudo chmod 777 /etc/systemd/system/syncthing@.service
# fi

echo "deb https://apt.syncthing.net/ syncthing release" > /etc/apt/sources.list.d/syncthing.list
sudo apt-get update -y
sudo apt-get install syncthing -y
syncthing --version

echo "[Unit]
Description=Syncthing - Open Source Continuous File Synchronization for %I
Documentation=man:syncthing(1)
After=network.target

[Service]
User=%i
ExecStart=/usr/bin/syncthing -no-browser -gui-address="0.0.0.0:8384" -no-restart -logflags=0
Restart=on-failure
SuccessExitStatus=3 4
RestartForceExitStatus=3 4

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/syncthing@.service
sudo systemctl daemon-reload
sudo systemctl status syncthing@root
sudo systemctl enable syncthing@root
sudo systemctl start syncthing@$USER
sudo systemctl enable syncthing@root
sudo systemctl start syncthing@root
#ss -antpl | grep 8384