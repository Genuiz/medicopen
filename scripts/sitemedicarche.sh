#!/bin/bash

#mise à jour du systeme 
sudo apt-get update

#installation, demerrage et activation du serveur apache
sudo apt-get install apache2 apache2-utils -y
sudo systemctl enable apache2
sudo systemctl start apache2

#installation du serveur et du client mysql server
sudo apt-get install mysql-client mysql-server -y

#Installation des dependances
sudo apt-get install php libapache2-mod-php php-mysql php-curl php-gd php-mbstring php-xml php-xmlrpc php-soap php-intl php-zip -y
sudo systemctl restart apache2

#Telechargement de wordpress depuis le site wordress, decompression et deplacement vers le repertoire des projets web
wget -c http://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz
sudo mv wordpress/* /var/www/html/
sudo chown -R www-data:www-data /var/www/html/
sudo chmod -R 755 /var/www/html/

#création de l'utilisateur admin, du mot de pass, de la base de donnée et attribution des privillège sur la db
sudo mysql -u root --execute  "CREATE USER 'admin'@'localhost' IDENTIFIED BY 'password';
CREATE DATABASE IF NOT EXISTS wp_myblog CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
GRANT ALL PRIVILEGES ON wp_myblog.* TO 'admin'@'localhost';
FLUSH PRIVILEGES; "


sudo mv /var/www/html/wp-config-sample.php /var/www/html/wp-config.php
sudo chmod 755 /var/www/html/wp-config.php

#https://tomjn.com/2014/03/01/wordpress-bash-magic/

#Configutation du fichier wp-config de wordpress pour permettre wordpress à acceder à la base de donnée
sudo sed -i "/DB_HOST/s/'[^']*'/'localhost'/2" /var/www/html/wp-config.php
sudo sed -i "/DB_NAME/s/'[^']*'/'wp_myblog'/2" /var/www/html/wp-config.php
sudo sed -i "/DB_USER/s/'[^']*'/'admin'/2" /var/www/html/wp-config.php
sudo sed -i "/DB_PASSWORD/s/'[^']*'/'password'/2" /var/www/html/wp-config.php
#grep DB_USER wp-config | awk -F\' '{print $4}'

sudo rm -rf /var/www/html/index.html

#activation et configuration du serveur virtuel apache pour que le site soit accecible depuis le port 9100
sudo touch /etc/apache2/sites-available/medicsite.conf
#En raison de test j'utilise le 777. Dans la prod je le channgerai en 755
sudo chmod 777 /etc/apache2/sites-available/medicsite.conf
echo "<VirtualHost *:9100>
  DocumentRoot /var/www/html/
  ServerName localhost
  
  <Directory /var/www/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
  </VirtualHost>"  > /etc/apache2/sites-available/medicsite.conf

#attribution des droits abusivement (755) pour faciliter la modification avec la commande sed
sudo chmod 777 /etc/apache2/ports.conf
sudo sed -i '$ a Listen 9100' /etc/apache2/ports.conf


sudo systemctl restart apache2.service 
sudo systemctl restart mysql.service 


