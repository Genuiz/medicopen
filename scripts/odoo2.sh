#!/bin/bash

#mise à jour du systeme et installation des outils
sudo apt update
sudo apt install git -y
sudo apt install python3-pip -y

#installation des dependances
sudo apt install -y build-essential wget python3-dev python3-venv \
python3-wheel libfreetype6-dev libxml2-dev libzip-dev libldap2-dev libsasl2-dev \
python3-setuptools node-less libjpeg-dev zlib1g-dev libpq-dev \
libxslt1-dev libldap2-dev libtiff5-dev libjpeg8-dev libopenjp2-7-dev \
liblcms2-dev libwebp-dev libharfbuzz-dev libfribidi-dev libxcb1-dev

#sudo su -c "useradd odoo -s /bin/bash -m"
#sudo chpasswd <<<"odoo:odoopw"

#creation de l'utilisateur odoo sans mot de passe
sudo adduser odoo --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password
#attribution d'un mot de passe à l'utilisateur odoo
echo "odoo:password" | sudo chpasswd


sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" >> /etc/apt/sources.list.d/pgdg.list'


#importation du  GPG Key pour le repository
wget -q https://www.postgresql.org/media/keys/ACCC4CF8.asc -O - | sudo apt-key add -
sudo apt install postgresql postgresql-contrib -y
sudo systemctl start postgresql
sudo systemctl enable postgresql

#sudo passwd postgres
sudo chpasswd <<<"postgres:odoopw"
#sudo su postgres
#createuser odoo
echo odoopw | su - postgres -c "createuser -s odoo"
sudo -u postgres -H -- psql -c "ALTER USER odoo WITH CREATEDB;"
#sudo -u postgres -H -- psql -d odoo -c "ALTER USER odoo WITH CREATEDB;"
sudo wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.bionic_amd64.deb
sudo apt install ./wkhtmltox_0.12.6-1.bionic_amd64.deb -y
sudo mkdir -p /opt/odoo/odoo
sudo chown -R odoo /opt/odoo
sudo chgrp -R odoo /opt/odoo
sudo su - odoo -c "git clone https://www.github.com/odoo/odoo --depth 1 --branch 14.0 /opt/odoo/odoo"
#echo admin | su - odoo -c "git clone https://www.github.com/odoo/odoo --depth 1 --branch 14.0 /opt/odoo/odoo"

#sudo su - odoo
#git clone https://www.github.com/odoo/odoo --depth 1 --branch 14.0 /opt/odoo/odoo
sudo chmod 777 /opt/odoo/
cd /opt/odoo
python3 -m venv odoo-venv
source odoo-venv/bin/activate
pip3 install wheel
pip3 install -r odoo/requirements.txt
deactivate
sudo mkdir /opt/odoo/odoo-custom-addons
sudo chmod 777 /opt/odoo/odoo-custom-addons
#exit

sudo touch /etc/odoo.conf
sudo chmod 777 /etc/odoo.conf
echo "[options]
; This is the password that allows database operations:
admin_passwd = StrongMasterPassword
db_host = False
db_port = False
db_user = odoo
db_password = False
addons_path = /opt/odoo/odoo/addons,/opt/odoo/odoo-custom-addons" > /etc/odoo.conf

sudo touch /etc/systemd/system/odoo.service
sudo chmod 777 /etc/systemd/system/odoo.service

echo "[Unit]
Description=Odoo14
Requires=postgresql.service
After=network.target postgresql.service

[Service]
Type=simple
SyslogIdentifier=odoo
PermissionsStartOnly=true
User=odoo
Group=odoo
ExecStart=/opt/odoo/odoo-venv/bin/python3 /opt/odoo/odoo/odoo-bin -c /etc/odoo.conf
StandardOutput=journal+console

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/odoo.service

sudo systemctl daemon-reload
sudo systemctl start odoo
sudo systemctl enable odoo
sudo systemctl status odoo

# detail password dans /etc/odoo.conf

echo "Reverse proxy configuration"
sudo apt install apache2 -y
sudo systemctl enable apache2
sudo a2enmod proxy
sudo a2enmod proxy_http

sudo touch  /etc/apache2/sites-available/odoo.conf
sudo chmod 777 /etc/apache2/sites-available/odoo.conf

echo "
ServerName localhost
      ErrorDocument 404 /404.html

      ProxyPass / http://127.0.0.1:8069/
      ProxyPassReverse / http://127.0.0.1:8069/

      ErrorLog ${APACHE_LOG_DIR}/odoo_error.log
      CustomLog ${APACHE_LOG_DIR}/odoo_access.log combined
" > /etc/apache2/sites-available/odoo.conf
sudo a2ensite odoo.conf
sudo service apache2 restart
