#!/bin/bash

#mise à jour du systeme 
sudo apt update
#installation de curl 
sudo apt-get install curl -y
#importation du  GPG Key pour le repository
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list

#le transport d'APT permet d'utiliser les dépôts auxquels on accède au moyen de HTTPS
sudo apt-get install apt-transport-https -y
sudo apt-get install syncthing -y
sudo systemctl start syncthing@$USER
sudo systemctl enable syncthing@$USER

#installation d'apache server
sudo apt install apache2 -y
sudo systemctl start apache2
sudo systemctl enable apache2


#activation des modules apache 
sudo a2enmod proxy proxy_http headers proxy_wstunnel

#attribution des droits abusivement (755) pour faciliter la modification avec la commande sed
sudo touch /etc/apache2/sites-available/syncthing.conf
sudo chmod 777 /etc/apache2/sites-available/syncthing.conf


echo "ServerName localhost
      ErrorDocument 404 /404.html

      ProxyPass / http://127.0.0.1:8384/
      ProxyPassReverse / http://127.0.0.1:8384/

      ErrorLog ${APACHE_LOG_DIR}/syncthing_error.log
      CustomLog ${APACHE_LOG_DIR}/syncthing_access.log combined" > /etc/apache2/sites-available/syncthing.conf

#activation et configuration du proxy pour que le site soit accecible depuis le port 9090
sudo a2ensite syncthing.conf
#sudo echo "Listen 9090" >> /etc/apache2/ports.conf
sudo sed -i '$ a Listen 9090' /etc/apache2/ports.conf
sudo systemctl reload apache2
sudo systemctl restart apache2

#afficher les ports ouvert dans la vm
sudo lsof -i -P -n | grep LISTEN
#sudo ufw allow 8080/tcp
