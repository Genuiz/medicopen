#!/bin/bash
#https://docs.openstack.org/install-guide/launch-instance-networks-provider.html
#https://docs.openstack.org/python-openstackclient/pike/cli/command-objects/router.html
#https://docs.openstack.org/python-openstackclient/latest/cli/command-objects/router.html

#https://docs.openstack.org/liberty/install-guide-ubuntu/launch-instance-networks-private.html important! 

#https://docs.openstack.org/python-openstackclient/ocata/command-objects/router.html

#https://connection.rnascimento.com/2021/03/08/openstack-single-node-microstack/

#microstack.openstack network create medicarche
microstack.openstack network create medicarche-private #--provider-network-type Flat
microstack.openstack network create  --share --external \
  --provider-physical-network medicarche-public \
  --provider-network-type flat medicarche-public
microstack.openstack subnet create arche --network medicarche-private --subnet-range 192.168.0.0/24
microstack.openstack subnet create arcade --network medicarche-private --subnet-range 192.168.100.0/24
microstack.openstack subnet create bastion --network medicarche-public --subnet-range 10.20.20.0/24 
 
microstack.openstack router create medrouter
#microstack.openstack router add route medicarche-public medrouter
#microstack.openstack router add subnet medrouter bastion #pour pointer vers l'exterieur


microstack.openstack router set medrouter --external-gateway medicarche-public

#microstack.openstack router set medrouter --external-gateway medicarche-public
microstack.openstack router add subnet medrouter arche
microstack.openstack router add subnet medrouter arcade
microstack.openstack router add subnet medrouter bastion
