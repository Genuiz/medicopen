#!/bin/bash
# https://www.jamescoyle.net/how-to/116-simple-apache-reverse-proxy-example

#mise à jour du systeme, installation d'apache
sudo apt update
sudo apt install apache2 -y

#Creation du server virtuel apache qui jouera le role de proxy pour que le site puisse etre atteint depuis le port 8086
sudo touch /etc/apache2/sites-available/nextcloud.conf
#En raison de test j'utilise le 777. Dans la prod je le channgerai en 755
sudo chmod 777 /etc/apache2/sites-available/nextcloud.conf
echo "<VirtualHost *:8086>
        ServerName localhost
        #ProxyPreserveHost On
        ProxyPass / http://$ip_vm2:9000/
        ProxyPassReverse / http://$ip_vm2:9000/
      </VirtualHost>"  > /etc/apache2/sites-available/nextcloud.conf

sudo sed -i '$ a Listen 8086' /etc/apache2/ports.conf

#activation des modules apaches et redemarrage du serveur 
sudo service apache2 reload
sudo a2ensite nextcloud.conf
sudo a2enmod proxy proxy_http headers proxy_wstunnel
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod env
sudo a2enmod dir
sudo a2enmod mime
sudo systemctl reload apache2
sudo systemctl restart apache2

#verifier si le port est bien exposé
sudo lsof -i -P -n | grep 8086

#exposer l'adresse et le port de la machine que l'on veut attaquer
#sudo iptables -t nat -A PREROUTING -p tcp --dport 3306 -j DNAT --to ip_distant:13306

#l'adresse de la db est le localhost:9000 cas nextcloud
