#!/bin/bash

#ligne de code permettant mettre en place un script non interactif 
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
sudo apt-get install -y -q
sudo apt-get install dialog apt-utils -y

# recuperation des credentials pour se connecter sur la conole Microstack
sudo snap get microstack config.credentials.keystone-password

#Clonage du systeme d'exploitation Ubuntu dans un repos dinstant afin de televerser l'OS dans Microstack
git clone https://gitlab.com/Genuiz/os-ubuntu-openstack.git
microstack.openstack image create --container-format bare --disk-format qcow2 --file ./os-ubuntu-openstack/focal-server-cloudimg-amd64.img ubuntu

#Creation des gabarits qui acceuilleront les vms contenant des applications
microstack.openstack flavor create m3.custom --id auto --ram 1024 --disk 5 --vcpus 2
microstack.openstack flavor create m4.custom --id auto --ram 1024 --disk 8 --vcpus 2
microstack.openstack flavor create m5.custom --id auto --ram 1024 --disk 9 --vcpus 2

microstack.openstack image list
microstack.openstack flavor list

#Creation du groupe de securité qui autorise un certain nombre des ports permettant aux applications d'etre atteint depuis le réseau privée ou internet  
microstack.openstack security group create private-sg
microstack.openstack security group rule create private-sg --ingress --protocol tcp --dst-port 8085:8085 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg --ingress --protocol tcp --dst-port 8086:8086 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg  --ingress --protocol tcp --dst-port 8087:8087 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg  --ingress --protocol tcp --dst-port 8088:8088 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg  --ingress --protocol tcp --dst-port 22:22 --remote-ip 0.0.0.0/0
microstack.openstack security group rule create private-sg  --ingress --protocol tcp --dst-port 80:80 --remote-ip 0.0.0.0/0

microstack.openstack security group rule create private-sg --protocol icmp private-sg
#microstack.openstack security group rule create private-sg --protocol tcp --src-ip 0.0.0.0/0 --dst-port 1:65525
microstack.openstack security group rule create private-sg --protocol tcp --remote-ip 0.0.0.0/0 --dst-port 1:65525
microstack.openstack security group rule list

#Creation de la pair de cle rsa pour securiser la connexion vms tournant sur Microstack de puis l'hote. 
ssh-keygen -q -C "" -N ""  -f open_key
sudo chown vagrant open_key
sudo chmod 400 open_key
microstack.openstack keypair create --public-key open_key.pub sto4_key
microstack.openstack keypair list

#Boucle permettant la création des vms 
for machine in `echo vm1`; do

    echo "Debut creation VM $machine"

    case $machine in

        vm1)
			microstack.openstack server create --network test --security-group private-sg --key-name sto4_key --flavor m4.custom --image ubuntu $machine
			microstack.openstack floating ip create external

			foo=`microstack.openstack floating ip list | egrep -e "None.*\|.*None"`

			if [ ! -z "$foo" ]; then

				#$value= echo "$foo" | head -n1 | awk '{print $1;}'
				bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
				# La variable bar contient l'adresse ip flottante
				echo "Floating IP = $bar"
			else
				echo "Error"
				exit 1
			fi

            ip_vm1=$bar; echo $ip_vm1;;

      
        # vm5)
		# 	microstack.openstack server create --network test --security-group private-sg --key-name sto4_key --flavor m3.custom --image ubuntu $machine
		# 	microstack.openstack floating ip create external

		# 	foo=`microstack.openstack floating ip list | egrep -e "None.*\|.*None"`

		# 	if [ ! -z "$foo" ]; then

		# 		#$value= echo "$foo" | head -n1 | awk '{print $1;}'
		# 		bar=`echo " $foo" | cut -d '|' -f 3 | sed 's/ //g'`
		# 		# La variable bar contient l'adresse ip flottante
		# 		echo "Floating IP = $bar"
		# 	else
		# 		echo "Error"
		# 		exit 1
		# 	fi
        
        #   ip_vm5=$bar; echo $ip_vm5;;

        *)
            echo "Error";;
    esac
	 microstack.openstack server add floating ip $machine $bar
    echo "Je dors pendant 300s"
    sleep 300
    #Ce script permet aux vms d'etre connu sans intervention humaine
    echo "ubuntu@$bar"
    ssh -o StrictHostKeyChecking=no -o PasswordAuthentication=no root@$bar
    sshpass -f password.txt ssh-copy-id  -i ./open_key.pub root@"$bar"
    sudo cat ./open_key.pub | ssh -i open_key ubuntu@"$bar" "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
    ssh -o StrictHostKeyChecking=no -o PasswordAuthentication=no ubuntu@$bar
    sshpass -f password.txt ssh-copy-id  -i ./open_key.pub ubuntu@"$bar"
    sudo cat ./open_key.pub | ssh -f open_key ubuntu@"$bar" "mkdir -p ~/.ssh && cat >> ~/.ssh/authorized_keys"
 
    echo "Fin de la creation VM $machine"
done


#Installation des applications et mise en place du reverse proxy qui permet requête d'etre acheminé de internet vers la vm
for machine in `echo vm1`; do 

    case $machine in

        vm1) 
            echo "reverse proxy syncthing"
            export ip_vm1;
            ./reverse_proxy_syncthing.sh; 
            echo "Installation Syncthing"
            scp -i open_key scripts/syncthing2.sh  ubuntu@$ip_vm1:/tmp; 
            ssh -i open_key ubuntu@$ip_vm1 "chmod +x /tmp/syncthing2.sh";
            ssh -i open_key ubuntu@$ip_vm1 "source /tmp/syncthing2.sh";;
            
        *)
          echo "Unknown" ;;
    esac
done






