#!/bin/bash
#https://lucasvidelaine.wordpress.com/2018/02/05/installation-de-devstack/
#https://gist.github.com/nyzm/9595516
#https://github.com/openstack/octavia/blob/master/devstack/samples/singlenode/local.conf
#https://github.com/openstack/trove-dashboard

#Prerequis DevStack
sudo apt-get --yes update
sudo apt-get install --yes sudo vim vim-nox lynx zip binutils wget
sudo apt-get install --yes openssl ssl-cert ssh
sudo apt-get install bridge-utils
sudo apt-get install --yes python3-pip
sudo apt-get install --yes ntp
pip install --upgrade pip
pip install -U os-testr

sudo useradd -s /bin/bash -d /opt/stack -m stack
sudo chmod +x /opt/stack
echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack

sudo cp /home/vagrant/devstack.sh /opt/stack
sudo -u stack -i git clone https://opendev.org/openstack/devstack

sudo -u stack -i cd devstack 
sudo -u stack -i touch /opt/stack/devstack/local.conf
sudo -u stack -i chmod 777 /opt/stack/devstack/local.conf
sudo echo "
[[local|localrc]]
RECLONE=False
HOST_IP=192.168.33.254



##########
# Manila #
##########
enable_plugin manila https://opendev.org/openstack/manila
enable_plugin manila-ui https://opendev.org/openstack/manila-ui

##########
# Heat   #
##########
enable_plugin heat https://git.openstack.org/openstack/heat
enable_plugin heat-dashboard https://opendev.org/openstack/heat-dashboard
enable_service h-eng h-api h-api-cfn h-api-cw

# # ##########
# # # Trove  #
# # ##########
# enable_plugin trove https://opendev.org/openstack/trove
# enable_plugin trove-dashboard https://opendev.org/openstack/trove-dashboard

#enable_plugin octavia https://opendev.org/openstack/octavia
#enable_plugin octavia-dashboard https://opendev.org/openstack/octavia-dashboard

# # ############
# # # Octavia  #
# # ############
# enable_plugin t
#enable_plugin neutron-lbaas https://git.openstack.org/openstack/neutron-lbaas
#enable_plugin octavia https://git.openstack.org/openstack/octavia.git


# enable_plugin neutron-lbaas-dashboard https://opendev.org/openstack/neutron-lbaas-dashboard

# ############
# # Barbican #
# ############
#enable_plugin barbican https://opendev.org/openstack/barbican
#enable_plugin barbican_ui https://github.com/openstack/barbican-ui



DATABASE_PASSWORD=password
ADMIN_PASSWORD=password
SERVICE_PASSWORD=password
SERVICE_TOKEN=password
RABBIT_PASSWORD=password

USE_PYTHON3=True
IPV4_ADDRS_SAFE_TO_USE=192.168.50.0/24
FIXED_RANGE=192.168.50.0/24
NETWORK_GATEWAY=192.168.50.1
FLOATING_RANGE=172.30.5.0/24
PUBLIC_NETWORK_GATEWAY=172.30.5.1

# Pre-requisites
#ENABLED_SERVICES=rabbit,mysql,key

# FLAT_INTERFACE=eth2
# PUBLIC_INTERFACE=eth2

# # Nova
# enable_service n-api
# enable_service n-cpu
# enable_service n-cond
# enable_service n-sch
# enable_service n-api-meta
# enable_service placement-api
# enable_service placement-client

# # Glance
# enable_service g-api
# enable_service g-reg

# # Cinder
# enable_service cinder
# enable_service c-api
# enable_service c-vol
# enable_service c-sch


# Swift
#ENABLED_SERVICES+=,swift
#SWIFT_HASH=66a3d6b56c1f479c8b4e70ab5c2000f5
#SWIFT_REPLICAS=1

#ENABLED_SERVICES+=,octavia,o-api,o-cw,o-hk,o-hm,o-da
#ENABLED_SERVICES+=,q-lbaasv2,octavia,o-cw,o-hk,o-hm,o-api
" > /opt/stack/devstack/local.conf

sudo -u stack -i /opt/stack/devstack/./stack.sh
