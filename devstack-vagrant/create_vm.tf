resource "openstack_compute_keypair_v2" "test-keypair" {
  name       = "my-keypair"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDe7lgJHVhdkLd1ALYxrVdqlAOehPZSwlnlx7zOZlW2c+Fjc1fx0r7v6hV6sgQluOdjNJtK1Yy6zao1KNp4SskgwrAI86pZ8dvSGXKk/QG4SXqGmzLLSmFFjWF5XPnrmz82AFlJHZ/IUSBkzMdBxWvNsLIfPmUOe5MUOy9QRDBYm9134Y3q8guL/EXMoydXXDB2L7M8PRfTPqD5KhP47/orZR7f1t3IgoVGjHPMIX/8Nw3LVCK99+8HZ4pgJXVmDvMvzJm/P03ro7jGYraIEAXK+aPLW7D4M9/fT8Tz3rBSPd4dpWMO4w1csWgk7/Fio5R3aO7YNilufzkdIjcM+eov53ONCv0naRhB0QG2Dv2EBbNM+QSf9ttSfwYwfSEwub5ZbQoy+JiCijqxS0hhoZOWWQWw1rtHfq5nxN3mD+mlAaHHXJ4jXZ4yTMgsHvcB25FP7KRTCH1ng5oCIZIEQ+LTag9Fw/Zz7duc028j9/nNlyc+/OC3pQghkF7o8S9eKvk= MLGA-06175+Stagiaire@MLGA-06175"
}

# resource "openstack_compute_instance_v2" "vm_odoo" {
#   name            = "odoo"
#   image_name      = "cirros-0.5.2-x86_64-disk"
#   flavor_name     = "m1.tiny"
#   key_pair        = "${openstack_compute_keypair_v2.test-keypair.name}"
#   security_groups = ["default"]

#   network {
#     name = "shared"
#   }
# }

resource "openstack_networking_network_v2" "medicarche_public" {
  name           = "medicarche_public"
  admin_state_up = "true"
  #physical_network = "public"
  external = true
  #network_type = "flat"
}

resource "openstack_networking_subnet_v2" "subnet_bastion" {
  name       = "bastion"
  network_id = "${openstack_networking_network_v2.medicarche_public.id}"
  cidr       = "172.30.5.0/24"
  ip_version = 4
}

resource "openstack_networking_network_v2" "medicarche_private" {
  name           = "medicarche_private"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "subnet_arcade" {
  name       = "arcade"
  network_id = "${openstack_networking_network_v2.medicarche_private.id}"
  cidr       = "192.168.199.0/24"
  ip_version = 4
}

resource "openstack_compute_secgroup_v2" "secgroup_medicarche" {
  name        = "secgroup_medicarche"
  description = "a security group for medicarche infra"

  rule {
    from_port   = 22
    to_port     = 22
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

resource "openstack_networking_port_v2" "port_1" {
  name               = "port_1"
  network_id         = "${openstack_networking_network_v2.medicarche_private.id}"
  admin_state_up     = "true"
  security_group_ids = ["${openstack_compute_secgroup_v2.secgroup_medicarche.id}"]

  fixed_ip {
    subnet_id  = "${openstack_networking_subnet_v2.subnet_arcade.id}"
    ip_address = "192.168.199.10"
  }
}

resource "openstack_compute_instance_v2" "instance_1" {
  name            = "instance_1"
  image_name      = "Fedora-Cloud-Base-36-1.5.x86_64"
  flavor_name     = "ds2G"
  key_pair        = "${openstack_compute_keypair_v2.test-keypair.name}"
  security_groups = ["${openstack_compute_secgroup_v2.secgroup_medicarche.name}"]
  user_data = "sudo dnf install httpd -y"

  network {
    port = "${openstack_networking_port_v2.port_1.id}"
  }
}


resource "openstack_networking_router_v2" "router_1" {
  name                = "my_router"
  admin_state_up       = true
  #external_network_id = "${openstack_networking_network_v2.medicarche_public.id}"
  external_network_id = "fc1bd1a3-dde8-442e-a248-1ddee3896f8e"
}

resource "openstack_networking_router_interface_v2" "router_interface_1" {
  router_id = "${openstack_networking_router_v2.router_1.id}"
  subnet_id = "${openstack_networking_subnet_v2.subnet_arcade.id}"
}



