#!/bin/bash

sudo useradd --no-create-home --shell /bin/false node_exporter

#Node_exporter installation
wget https://github.com/prometheus/node_exporter/releases/download/v1.4.0/node_exporter-1.4.0.linux-amd64.tar.gz
sudo tar -xvf node_exporter-*.*-amd64.tar.gz

#cd node_exporter-1.4.0.linux-amd64
sudo cp node_exporter-1.4.0.linux-amd64/node_exporter /usr/local/bin

# Creating Node Exporter Systemd service
#cd /lib/systemd/system
sudo touch /lib/systemd/system/node_exporter.service
sudo chmod 556 /lib/systemd/system/node_exporter.service
sudo echo "[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target
[Service]
Type=simple
User=node_exporter
Group=node_exporter
ExecStart=/usr/local/bin/node_exporter
Restart=always
RestartSec=10s
[Install]
WantedBy=multi-user.target" > /lib/systemd/system/node_exporter.service

sudo systemctl daemon-reload
sudo systemctl enable node_exporter
sudo systemctl start node_exporter
