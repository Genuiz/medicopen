#!/bin/bash
# https://www.jamescoyle.net/how-to/116-simple-apache-reverse-proxy-example
sudo apt update
sudo apt install apache2 -y
sudo touch /etc/apache2/sites-available/medicsite.conf
#En raison de test j'utilise le 777. Dans la prod je le channgerai en 755
sudo chmod 777 /etc/apache2/sites-available/medicsite.conf

#Creation du server virtuel apache qui jouera le role de proxy pour que le site puisse etre atteint depuis le port 8088
echo "<VirtualHost *:8088>
        ServerName localhost
        #ProxyPreserveHost On
        ProxyPass / http://$ip_vm4:9100/
        ProxyPassReverse / http://$ip_vm4:9100/
      </VirtualHost>"  > /etc/apache2/sites-available/medicsite.conf

sudo sed -i '$ a Listen 8088' /etc/apache2/ports.conf

#activation des modules apaches et redemarrage du serveur 
sudo service apache2 reload
sudo a2ensite medicsite.conf
sudo a2enmod proxy proxy_http headers proxy_wstunnel
sudo a2enmod rewrite
sudo a2enmod headers
sudo a2enmod env
sudo a2enmod dir
sudo a2enmod mime
sudo systemctl reload apache2
sudo systemctl restart apache2
sudo lsof -i -P -n | grep 8088

