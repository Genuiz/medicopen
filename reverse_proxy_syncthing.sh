#!/bin/bash

# https://kirelos.com/how-to-install-syncthing-on-debian-desktop-server/

#Reverse proxy dans la VM
sudo apt update
sudo apt install apache2 -y
sudo sed -i '5,1d' /etc/apache2/ports.conf #Delete Listen 80 in apache
sudo systemctl restart apache2
sudo touch /etc/apache2/sites-available/syncthing.conf
sudo chmod 777 /etc/apache2/sites-available/syncthing.conf
sudo a2ensite syncthing.conf
sudo systemctl restart apache2
sudo sed -i '$ a Listen 8085' /etc/apache2/ports.conf
#sudo ufw allow 8085/tcp
#systemctl status ufw
sudo systemctl restart apache2
sudo lsof -i -P -n | grep LISTEN

echo "<VirtualHost *:8085>
        ServerName localhost
        #ProxyPreserveHost On
        ProxyPass / http://$ip_vm1:9090/
        ProxyPassReverse / http://$ip_vm1:9090/
      </VirtualHost>" > /etc/apache2/sites-available/syncthing.conf

sudo a2ensite syncthing.conf     
sudo a2enmod proxy proxy_http headers proxy_wstunnel
sudo systemctl restart apache2
sudo lsof -i -P -n | grep LISTEN

# #Reverse proxy dans openstack
# sudo apt update
# sudo apt-get install curl -y
# curl -s https://syncthing.net/release-key.txt | sudo apt-key add -
# echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list
# sudo apt-get install apt-transport-https -y
# sudo apt-get install syncthing -y
# sudo systemctl start syncthing@$USER
# sudo systemctl enable syncthing@$USER
# sudo apt install apache2 -y
# sudo systemctl start apache2
# sudo systemctl enable apache2
# sudo a2enmod proxy proxy_http headers proxy_wstunnel

# echo "ServerName 192.168.33.16
#       ErrorDocument 404 /404.html

#       ProxyPass / http://127.0.0.1:8384/
#       ProxyPassReverse / http://127.0.0.1:8384/

#       ErrorLog ${APACHE_LOG_DIR}/syncthing_error.log
#       CustomLog ${APACHE_LOG_DIR}/syncthing_access.log combined" > /etc/apache2/sites-available/syncthing.conf

# sudo a2ensite syncthing.conf
# sudo echo "Listen 8085" >> /etc/apache2/ports.conf
# sudo systemctl restart apache2
# sudo systemctl reload apache2
# sudo systemctl restart apache2
# sudo lsof -i -P -n | grep LISTEN
# #sudo ufw allow 8080/tcp
